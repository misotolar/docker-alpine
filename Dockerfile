FROM alpine:latest
MAINTAINER Michal Sotolar <michal@sotolar.com>

RUN apk add --no-cache bash curl git jq
